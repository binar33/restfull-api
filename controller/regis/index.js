const { Users, Biodatas, Scores } = require("../../models");
const bcrypt = require("bcrypt");

exports.regis = async (req, res) => {
    const payload = req.body
  const { username, email } = req.body;

  const encryptPassword = await bcrypt.hash(payload.password, 10);
  payload.password = encryptPassword;

  const validateEmailUser = await Users.findOne({
    where: { email },
  });

  const validateUsernameUser = await Users.findOne({
    where: { username },
  });

  if (validateEmailUser) {
    res.status(401).json({
      result: "failed",
      message: "email sudah ada",
    });
    return;
  }
  if (validateUsernameUser) {
    res.status(402).json({
      result: "failed",
      message: "username sudah ada",
    });
    return;
  }

  const regisUser = await Users.create(payload);
  const regisBio = await Biodatas.create({
    name: payload.name,
    gender: payload.gender,
    address: payload.address,
    social_media_url: payload.social_media_url,
    user_id: regisUser.id,
  });
  const regisScore = await Scores.create({
    biodata_id: regisBio.id,
    game_id: 1,
    score: 0,
  });

  res.send({
    name: regisBio.name,
    username: regisUser.username,
    email: regisUser.email,
    address: regisBio.address,
    gender: regisBio.gender,
    social_media_url: regisBio.social_media_url,
  });
};
