const { Games } = require("../../models");

exports.game = async (req, res) => {
  try {
    const data = await Games.findAll();
    res.send({ data });
  } catch (error) {
    res.send(error);
  }
};

exports.gamedetail = async (req, res) => {
  try {
    const name = req.params.name;
    const data = await Games.findOne({ where: { name } });
    res.send({ data });
  } catch (error) {
    res.send(error);
  }
};
