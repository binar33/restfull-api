const { Scores, Biodatas } = require("../../models");

exports.scoreUser = async (req, res) => {
  const bioUser = await Biodatas.findOne({
    where: { user_id: req.user.id },
  });

  const scoreUser = await Scores.findOne({
    where: { biodata_id: bioUser.id },
  });

  if (!scoreUser) {
    const newScoreUser = await Scores.create({
      biodata_id: bioUser.id,
      game_id: 1,
      score: 0,
    });
    res.json({
      message: "SUCCESS",
      data: newScoreUser,
    });
  } else {
    res.json({
      message: "SUCCESS",
      data: scoreUser,
    });
  }
};

exports.updateScore = async (req, res) => {
  const bioUser = await Biodatas.findOne({
    where: { user_id: req.user.id },
  });

  if (!bioUser) {
    res.status(404).json({
      message: "FAILED",
    });
    return;
  }

  const scoreUser = await Scores.findOne({
    where: { biodata_id: bioUser.id },
  });

  if (!scoreUser) {
    res.status(404).json({
      message: "FAILED",
    });
    return;
  }

  await scoreUser.update({ score: Number(req.body.score) });
  await scoreUser.save();

  res.json(scoreUser);
};

exports.leaderboard = async (req, res) => {
  const bioUser = await Biodatas.findAll({
    include: Scores,
  });

  const compare = (a, b) => {
    if (Number(a.Score.score) > Number(b.Score.score)) {
      return -1;
    }
    if (Number(a.Score.score) < Number(b.Score.score)) {
      return 1;
    }
    return 0;
  };

  const newBio = bioUser.sort(compare);
  res.json({
    message: "SUCCESS",
    data: newBio,
  });
};
