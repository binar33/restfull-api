const { Users, Biodatas } = require("../../models");

exports.user = async (req, res) => {
  const user = await Users.findOne({
    where: { id: req.user.id },
    include: Biodatas,
  });

  if (!user) {
    res.status(401).json({
      message: "Not find User!",
    });
    return;
  }

  const { name, gender, address, social_media_url } = user.Biodata;
  const { email, username } = user;
  const payload = {
    name,
    gender,
    address,
    email,
    username,
    social_media_url,
  };

  res.status(200).json({
    data: payload,
    message: "Success",
  });
};

exports.UpdateUser = async (req, res) => {
  try {
    const userUpdate = await Users.findOne({
      where: { id: req.user.id },
      include: Biodatas,
    });

    await userUpdate.update(req.body);
    await userUpdate.Biodata.update(req.body);
    await userUpdate.save();

    const { email, username } = userUpdate;
    const { name, gender, address, social_media_url } = userUpdate.Biodata;

    res.json({
      message: "SUKSES",
      data: {
        name,
        email,
        username,
        gender,
        address,
        social_media_url,
      },
    });
  } catch (error) {
    res.status(404).json({
      message: "FAILED",
    });
  }
};
