'use strict';
const bcrypt = require("bcrypt");

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert("Users", [
      {
        email: "ewa.el@gmail.com",
        username: "ewaa",
        password: await bcrypt.hash("ewa123", 10),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        email: "san@gmail.com",
        username: "sansan",
        password: await bcrypt.hash("sansan123", 10),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down (queryInterface, Sequelize) {
     return queryInterface.bulkDelete("Users", null, {});
  }
};
