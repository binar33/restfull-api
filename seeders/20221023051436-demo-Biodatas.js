'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert("Biodatas", [
      {
        name: "Ewa Elfany",
        gender: "female",
        address: 'Medan',
        user_id: 1,
        social_media_url: "https://blabla.com",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Gea Sansan",
        gender: "female",
        address: 'Bandung',
        user_id: 2,
        social_media_url: "https://bloblo.com",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete("Biodatas", null, {});
  }
};
