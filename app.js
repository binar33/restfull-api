const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const passport = require("./lib/passport");
const authenticationController = require("./controller/authentication");
const gameListController = require("./controller/gamelist");
const userController = require("./controller/user");
const gameController = require("./controller/game");
const regisController = require("./controller/regis");

const app = express();
const port = process.env.PORT || 8082;

app.use(cors());
app.use(bodyParser.json());
app.use(passport.initialize());

app.post("/login", authenticationController.login);
app.get("/gamelist", gameListController.game);
app.get("/game-list/detail/:name", gameListController.gamedetail);

app.post("/registrasi", regisController.regis);

app.get(
  "/user",
  passport.authenticate("jwt", { session: false }),
  userController.user
);
app.post(
  "/update-user",
  passport.authenticate("jwt", { session: false }),
  userController.UpdateUser
);

app.get(
  "/score-user",
  passport.authenticate("jwt", { session: false }),
  gameController.scoreUser
);

app.post(
  "/score-user",
  passport.authenticate("jwt", { session: false }),
  gameController.updateScore
);
app.get("/leaderboard", gameController.leaderboard);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
